/**
 * Inline Mesages
 * check for form errors & move .messages inline with the form
 */

if (Drupal.jsEnabled) {
  $(document).ready(function(){
    
    // remove #form-path that's appended to the form action so that we can jump
    // to the form and not the anchor
    $('form').each(function() {
      if ($(this).attr('action')) {
        var action = $(this).attr('action').split('#');
        $(this).attr('action', action[0]);
      }
    });
    
    // move messages
    if (Drupal.settings.form_submitted) {
      $form_id = $('form#' + Drupal.settings.form_submitted);
      
      var msg = $('.messages');
      if (msg.length) {
        $form_id.before(msg.attr('id', 'inline-messages'));
        $settings = Drupal.settings.inline_messages_scrollto;
        $.scrollTo($form_id, $settings['duration'], {offset:$settings['offset']});
      }
    }
    
  });
}